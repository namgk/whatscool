export default class CoolAPI {
  private c: Number

  constructor(c: Number) {
    this.c = c
  }

  public setC(c: Number){
    this.c = c
  }
}