import {expect} from 'chai';
import CoolAPI from '../src/coolApi'
describe("Test Flows", function () {
  var coolApi
  before(function () {
    coolApi = new CoolAPI(1)
    expect(coolApi.c).to.equal(1)
  })

  it('test', function(){
    coolApi.setC(2)
    expect(coolApi.c).to.equal(2)
  })
})